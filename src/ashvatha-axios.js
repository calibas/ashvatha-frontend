import axios from 'axios'

class AshvathaAxios {
    constructor() {
        this.siteURL = 'https://ashvatha.org/d8/web/';

        this.paths = {
            'newsList': 'api/news',
            'newsItem': 'api/news/{id}',
            'educationList': 'api/education',
            'educationItem': 'api/education/{id}',
            'groups': 'api/groups',
            'group': 'api/group/{id}',
            'joinGroup': 'api/group/{id}/join',
            'leaveGroup': 'api/group/{id}/leave',
            'groupAddNode': 'api/groupadd',
            'csrf': 'rest/session/token',
            'myInfo' : 'api/myinfo',
            'userInfo': 'api/user/{id}',
            'userImageUpload': 'file/upload/node/article/field_image',
            //'userImageUpload' : 'file/upload/user/user/user_picture'
            'userEdit': 'user/{id}',
            'userRegister': 'user/register',
            'userLogin': 'user/login',
            'stats': 'api/stats',
            'createNode': 'node'

        };

        // this.accessToken = localStorage.getItem('access_token');
        // this.getHeader = {
        //     Authorization: `Bearer ${this.accessToken}`
        // };
    }

    get(path, id = 0) {
        let url = this.paths[path].replace('{id}', id);
        return axios.get(this.siteURL + url + '?_format=json', {withCredentials: true});
        //return axios.get(this.siteURL + url + '?_format=json', {headers: this.getHeader});
    }

    post(path, data = '', id = 0) {
        const reqHeader = {
            //'X-CSRF-Token': sessionStorage.getItem('csrf_token')
            'X-CSRF-Token': getCookie('csrf_token')
        };
        let url = this.paths[path].replace('{id}', id);
        return axios.post(this.siteURL + url + '?_format=json', data, {headers: reqHeader, withCredentials: true });
    }

    // This is handled separately because of the token query string
    logout(logoutToken) {
        return axios.post(this.siteURL + 'user/logout?_format=json&token=' + logoutToken, '', {withCredentials: true });
    }

    patch(path, data = '', id = 0) {
        const reqHeader = {
            //'X-CSRF-Token': sessionStorage.getItem('csrf_token')
            'X-CSRF-Token': getCookie('csrf_token')
        };
        let url = this.paths[path].replace('{id}', id);
        return axios.patch(this.siteURL + url + '?_format=json', data, {headers: reqHeader, withCredentials: true});
    }

    getCsrfToken() {
        return new Promise((resolve, reject) => {
            this.get('csrf').then((res) => {
                console.log(res);
                //sessionStorage.setItem('csrf_token', res.data);
                setCookie('csrf_token', res.data, 2000000);
                resolve();
            }).catch((err) => {
                console.log(err.response);
                reject();
            })
        })
    }

    upload(path, file) {
        let data = new FormData();
        data.append('file', file);
        let reqHeader = {
            //Authorization: `Bearer ${this.accessToken}`,
            'Content-Type': 'application/octet-stream',
            'Content-Disposition': 'file; filename="' + file.name + '"',
            'X-CSRF-Token': getCookie('csrf_token')

        };
        let config = {
            headers: reqHeader,
            withCredentials: true,
            onUploadProgress(progressEvent) {
                console.log(Math.round((progressEvent.loaded * 100) / progressEvent.total));
            }
        };
        return axios.post(this.siteURL + this.paths[path] + '?_format=json', file, config)

    }

}

// Modified from https://www.w3schools.com/js/js_cookies.asp
function setCookie(cname, cvalue, exseconds) {
    var d = new Date();
    d.setTime(d.getTime() + (exseconds * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export default new AshvathaAxios();