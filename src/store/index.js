import Vue from 'vue'
import Vuex from 'vuex'
//import axios from 'axios'
import ashvathaAxios from '../ashvatha-axios'
import user from './modules/user'
import news from './modules/news'
import education from './modules/education'
import groups from './modules/groups'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        user,
        news,
        education,
        groups
    },
    state: {
        isMobile: true,
        // Used for
        loggedIn: false,
        //csrfToken: ''
    },
    mutations: {
        setMobile(state, payload) {
            state.isMobile = payload;
        },
        setLoggedIn(state) {
            state.loggedIn = true;
        },
        setLoggedOut(state) {
            state.loggedIn = false;
        },
        //setCsrfToken(state, payload) {
        //    state.csrfToken = payload;
        //}
    },
    actions: {
        login(context, payload) {
            localStorage.setItem('access_token', payload.access_token);
            if (payload.refresh_token) {
                localStorage.setItem('refresh_token', payload.refresh_token);
            }
            localStorage.setItem('last_access', Math.floor(Date.now() / 1000));
            context.commit('setLoggedIn');

        },
        logout(context) {
            return new Promise((resolve, reject) => {
                let logoutToken = this._vm.$cookies.get('logout_token');
                ashvathaAxios.logout(logoutToken).then(() => {
                    context.dispatch('user/getMyInfo');
                      resolve();
                }).catch((err) => {
                    console.log(err);
                    reject();
                });
            });
        },
        checkLogin: function (context) {
            context.dispatch('user/getMyInfo');
            /*
            //Get Oauth refresh and access tokens
            const refreshToken = localStorage.getItem('refresh_token') || '';
            const accessToken = localStorage.getItem('access_token') || '';

            //Debug
            console.log('Refresh token: ' + refreshToken);
            console.log('Access token: ' + localStorage.getItem('access_token'));
            console.log('Last access: ' + localStorage.getItem('last_access'));
            console.log('Current timestamp: ' + Math.floor(Date.now() / 1000));

            //Extract user id from JWT token
            const base64Url = accessToken.split('.')[1];
            const base64 = base64Url.replace('-', '+').replace('_', '/');
            const parsedJWT = JSON.parse(window.atob(base64));
            console.log(parsedJWT.sub);
            context.commit('user/setID', {id: parsedJWT.sub});


            if (!refreshToken) {
                context.commit('setLoggedOut');
                return false;
            }
            // Check if access token has been updated in the past day (86400 seconds)
            if (parseInt(localStorage.getItem('last_access')) + 86400 < Math.floor(Date.now() / 1000)) {
                context.dispatch('refreshAccessToken').then(() => {
                    context.dispatch('user/getMyInfo');
                });
            } else {
                context.dispatch('user/getMyInfo');
            }
            */

        },
        /*
        refreshAccessToken(context) {
            console.log('Refreshing token...');
            var oauthURL = 'https://ashvatha.org/d8/web/oauth/token';

            var loginData = new FormData();
            loginData.set('grant_type', 'refresh_token');
            loginData.set('client_id', '6bfebb70-4d67-4ced-8618-db72da2ece32');
            loginData.set('client_secret', '!pztD5z%');
            loginData.set('refresh_token', localStorage.getItem('refresh_token'));

            axios.post(oauthURL, loginData).then((res) => {
                console.log(res.data);
                let access_token = res.data.access_token;
                let refresh_token = res.data.refresh_token;
                context.dispatch('login', {access_token, refresh_token});
            }).catch((err) => {
                console.log(err.response);
                context.dispatch('logout');
            });
        },
        */
        /*
        getCsrfToken(context) {
            ashvathaAxios.get('csrf').then((res) => {
                console.log(res);
                //context.commit('setCsrfToken', res.data);
                sessionStorage.setItem('csrf_token', res.data);
            }).catch((err) => {
                console.log(err.response);
            })
        }
        */
    },
    strict: debug
})