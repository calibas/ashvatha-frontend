//import shop from '../../api/shop'

// initial state
const state = {
    name: '',
    id: 0
    // all: []
};

// getters
const getters = {
    getName(state) {
        return state.name;
    },
    getID(state) {
        return state.id;
    }
};

// actions
const actions = {
    // getAllProducts({commit}) {
    //     shop.getProducts(products => {
    //         commit('setProducts', products)
    //     })
    // }
};

// mutations
const mutations = {
    setName(state, name) {
        state.name = name;
    },
    setID(state, id) {
        state.id = id;
    }
    // setProducts(state, products) {
    //     state.all = products
    // },

    // decrementProductInventory(state, {id}) {
    //     const product = state.all.find(product => product.id === id);
    //     product.inventory--
    // }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}