/* eslint-disable no-console */
import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import VueCookies from 'vue-cookies'
//import axios from 'axios'
import Notifications from 'vue-notification'
import store from './store'
import {routes} from './routes';

Vue.use(VueRouter);
Vue.use(VeeValidate);
Vue.use(Notifications);
Vue.use(VueCookies);

//Vue.prototype.$http = axios;

Vue.config.productionTip = false;

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
