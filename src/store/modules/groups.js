//import axios from 'axios';
import ashvathaAxios from '../../ashvatha-axios'

const state = {
    groups: [
        // {
        //     name: 'Loading...',
        //     website: '',
        //     description: 'Loading...',
        //     image: '',
        //     id: '0'
        // }
    ],
    memberCount: 0,
    group: {},
    members: [],
    posts: []
};

const getters = {
    getGroups(state) {
        return state.groups;
    },
    getGroup(state) {
        return state.group;
    },
    getMemberCount(state) {
        return state.memberCount;
    },
    getPosts(state) {
        return state.posts;
    },
    getMembers(state) {
        return state.members
    }
};

// actions
const actions = {
    getGroupList(context) {
        context.commit('saveGroups', []);

        //Formerly: axios.get('https://ashvatha.org/d8/web/api/groups?_format=json', {headers: reqHeader}).then((res) => {
        ashvathaAxios.get('groups').then((res) => {
            let groups = [];
            console.log(res.data);
            res.data.forEach((responseData) => {
                let website = (responseData.website[0]) ? responseData.website[0].uri : '';
                let description = (responseData.description[0]) ? responseData.description[0].value : '';
                let image = (responseData.image[0]) ? responseData.image[0].uri : '/d8/web/sites/default/files/styles/medium/public/default_images/Ashvatha-Logo.png';
                const group = {
                    name: responseData.name,
                    website,
                    description,
                    image,
                    id: responseData.id,
                    userCount: responseData.userCount
                };
                groups.push(group);
            });
            console.log(groups);
            context.commit('saveGroups', groups);
        }).catch((err) => {
            console.log(err);
        });
    },
    getGroupInfo(context, id) {
        context.commit('saveGroup', {});

        //axios.get('https://ashvatha.org/d8/web/api/group/' + id + '?_format=json', {headers: reqHeader}).then((res) => {
        ashvathaAxios.get('group', id).then((res) => {
            let group = {
                name: res.data.name,
                website: (res.data.website[0]) ? res.data.website[0].uri : '',
                description: (res.data.description[0]) ? res.data.description[0].value : '',
                image: (res.data.image[0]) ? res.data.image[0].uri : '/d8/web/sites/default/files/styles/medium/public/default_images/Ashvatha-Logo.png',
                id: res.data.id,
                userCount: res.data.userCount,
                members: res.data.members,
                articles: res.data.articles,
                pages: res.data.pages
            };
            context.commit('setMemberCount', res.data.userCount);
            context.commit('saveGroup', group);
        }).catch((err) => {
            console.log(err);
        });
    },
    // fetchMembers(context, id) {
    //
    // },
    // fetchPosts(context, id) {
    //
    // },
    joinGroup(context, id) {
        return new Promise((resolve, reject) => {
            //axios.get('https://ashvatha.org/d8/web/api/group/' + id + '/join?_format=json', {headers: reqHeader}).then((res) => {
            ashvathaAxios.get('joinGroup', id).then((res) => {
                //context.dispatch('refreshAccessToken', {}, {root:true});
                context.dispatch('getGroupInfo', id);
                //context.commit('setMemberCount', context.getters['getMemberCount'] + 1);
                console.log(res);
                resolve();
            }).catch((err) => {
                console.log(err);
                reject();
            });
        });
    },
    leaveGroup(context, id) {
        return new Promise((resolve, reject) => {
            //axios.get('https://ashvatha.org/d8/web/api/group/' + id + '/leave?_format=json', {headers: reqHeader}).then((res) => {
            ashvathaAxios.get('leaveGroup', id).then((res) => {
                //context.dispatch('refreshAccessToken',{}, {root:true});
                context.dispatch('getGroupInfo', id);
                //context.commit('setMemberCount', context.getters['getMemberCount'] - 1);
                console.log(res);
                resolve();
            }).catch((err) => {
                reject();
                console.log(err);
            });
        });
    }
};

// mutations
const mutations = {
    saveGroups(state, groups) {
        state.groups = groups;
    },
    saveGroup(state, group) {
        state.group = group;
    },
    setMemberCount(state, count) {
        state.memberCount = count;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}