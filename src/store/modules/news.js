//import shop from '../../api/shop'
import ashvathaAxios from '../../ashvatha-axios'

// initial state
const state = {
    name: '',
    id: 0,
    newsList: [],
    newsItem: {}
    // all: []
};

// getters
const getters = {
    getName(state) {
        return state.name;
    },
    getID(state) {
        return state.id;
    },
    getNewsList(state) {
        return state.newsList;
    },
    getNewsItem(state) {
        return state.newsItem;
    }
};

// actions
const actions = {
    fetchNewsList(context) {
        context.commit('saveNewsList', []);
       ashvathaAxios.get('newsList').then((res) => {
           console.log(res.data);
           context.commit('saveNewsList', res.data);
        }).catch((err) => {
            console.log(err);
        });
    },
    getNewsInfo(context, id) {
        context.commit('saveNewsItem', {});
        return new Promise((resolve, reject) => {
            // Do something here... lets say, a http call using vue-resource
            //this.$http("/api/something").then(response => {
            ashvathaAxios.get('newsItem', id).then((res) => {
                // http success, call the mutator and change something in state
                context.commit('saveNewsItem', res.data);
                resolve();  // Let the calling function know that http is done. You may send some data back
            }, error => {
                // http failed, let the calling function know that action did not work out
                reject(error);
            })
        })
        // ashvathaAxios.get('newsItem', id).then((res) => {
        //     console.log(res.data);
        //     context.commit('saveNewsItem', res.data);
        // }).catch((err) => {
        //     console.log(err);
        // });
    }
};

// mutations
const mutations = {
    setName(state, name) {
        state.name = name;
    },
    setID(state, id) {
        state.id = id;
    },
    saveNewsList(state, newsList) {
        state.newsList = newsList;
    },
    saveNewsItem(state, newsItem) {
        state.newsItem = newsItem;
    }

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}