import Home from './components/Home.vue';
import Contact from './components/Contact.vue';
import About from './components/About';
import Login from './components/login/Login.vue';
import SignIn from './components/login/SignIn';
import SignUp from './components/login/SignUp';
import NewsMain from './components/news/MainPage';
import NewsInfo from './components/news/NewsInfo';
import GroupListing from './components/groups/GroupListing';
import GroupInfo from './components/groups/GroupInfo';
import Profile from './components/user/Profile';
import EditProfile from './components/user/EditProfile';
import Create from './components/create/Create';
import CreateArticle from './components/create/CreateArticle';
import NotFound from './components/NotFound.vue';

export const routes = [
    {path: '', name: 'home', component: Home},
    {path: '/vue', name: 'dev-home', component: Home},
    {path: '/contact', name: 'contact', component: Contact},
    {path: '/about', name: 'about', component: About},
    {path: '/login', name: 'login', component: Login,
        children: [
            {path: 'signin', name: 'signin', component: SignIn},
            {path: 'signup', name: 'signup', component: SignUp}
        ]},
    {path: '/news', name: 'news', component: NewsMain},
    {path: '/news/:id', name: 'news-item', component: NewsInfo},
    {path: '/groups', name: 'groups', component: GroupListing},
    {path: '/group/:id', name: 'group', component: GroupInfo},
    {path: '/user/:id', name: 'profile', component: Profile},
    {path: '/user/:id/edit', name: 'edit-profile', component: EditProfile},
    {path: '/create', name: 'create', component: Create,
        children:[
            {path: 'article', name: 'create-article', component: CreateArticle}
    ]},
    {path: '*', name: '404', component: NotFound}
];