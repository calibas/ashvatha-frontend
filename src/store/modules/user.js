// import axios from 'axios';
import ashvathaAxios from '../../ashvatha-axios'

const state = {
    name: '',
    id: 0,
    imageURL: '',
    myGroups: []
};

const getters = {
    getName(state) {
        return state.name;
    },
    getID(state) {
        return state.id;
    },
    getImage(state) {
        return state.imageURL;
    },
    getGroups(state) {
        return state.myGroups || [];
    }
};

const actions = {
    /*
    sendSignIn(context, payload ) {
        var oauthURL = 'https://ashvatha.org/d8/web/oauth/token';

        var loginData = new FormData();
        loginData.set('grant_type', 'password');
        loginData.set('client_id', '6bfebb70-4d67-4ced-8618-db72da2ece32');
        loginData.set('client_secret', '!pztD5z%');
        loginData.set('username', payload.username);
        loginData.set('password', payload.password);

        axios.post(oauthURL, loginData).then((res) => {
            console.log(res.data);
            let access_token = res.data.access_token;
            let refresh_token = res.data.refresh_token;
            context.dispatch('login', { access_token, refresh_token }, {root:true});
            context.dispatch('checkLogin', {}, {root:true});
            //router.go('home');
            console.log(this);
            this._vm.$notify({
                 group: 'alerts',
                 title: 'Login Success',
            //     text: 'Redirecting to previous page...',
                 type: 'success'
            });
        }).catch((err) => {
            this._vm.$notify({
                group: 'alerts',
                title: 'Login Failed',
                text: 'Please check your user name and password.',
                type: 'error'
            });
            console.log(err);
        });
    },
    */
    getMyInfo(context) {
        ashvathaAxios.get('myInfo').then((res) => {
            console.log(res);
            const userInfo = res.data;
            if (userInfo.uid > 0) {
                //context.commit('setID', {id: userInfo.uid});
                context.commit('setName', {name: userInfo.name});
                context.commit('setImageURL', {url: userInfo.user_picture});
                context.commit('setLoggedIn', {}, {root:true});
                context.commit('setGroups', userInfo.groups);
                context.commit('setID', userInfo.uid);
            } else {
                context.commit('setLoggedOut', {}, {root:true});
            }
        }).catch((err) => {
            console.log(err.response.data.message);
            this._vm.$notify({
                group: 'alerts',
                title: 'Error getting user info.',
                text: err.response.data.message,
                type: 'error'
            });
        });
    }
};

const mutations = {
    setName(state, {name}) {
        state.name = name;
    },
    setID(state, id) {
        state.id = id;
    },
    setImageURL(state, {url}) {
        state.imageURL = url;
    },
    setGroups(state, groups) {
        state.myGroups = groups;
    },
    addGroup(state, group) {
        state.myGroups.push(group);
    },
    removeGroup(state, id) {
        state.myGroups = state.myGroups.filter(item => item.id !== id)
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}